FROM node:14-alpine

#app workspace
RUN apk add ffmpeg
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

CMD [ "node","."]
